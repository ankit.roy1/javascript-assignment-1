const form  = document.getElementById('form');

function displayDetails(userData){
    let email=document.getElementById('emailDisplay')
    email.innerHTML=`Email  : ${userData.email}`
    let password=document.getElementById('passwordDisplay')
    password.innerHTML=`Password    : ${userData.password}`
    let role=document.getElementById('roleDisplay')
    role.innerHTML=`Role    : ${userData.role}`
    let gender=document.getElementById('genderDisplay')
    gender.innerHTML=`Gender    : ${userData.gender}`
    let permissions=document.getElementById('permissionDisplay')
    let perms=userData.permissions.join(' ,')
    permissions.innerHTML=`Permissions  : ${perms}`
}

function createAlert(id,message){
    let alert=document.getElementById("alert")
    alert.innerHTML=message;
}

function validateEmail(email) 
{
        let re = /\S+@\S+\.\S+/;
        return re.test(email);
}

function validatePassword(password){
    if(password.length < 6 ){
        createAlert("Password should be min 6 character with mix of Uppercase, lowercase and digits")
        return false;
    }

    if (password.search(/[0-9]/) === -1) {
        // console.log(password.search(/[0-9]/))
        createAlert("Password should be min 6 character with mix of Uppercase, lowercase and digits");
        return false;
    }
    if (password.search(/[a-z]/) === -1) {
        // console.log(password.search(/[a-z]/))
        createAlert("Password should be min 6 character with mix of Uppercase, lowercase and digits");
        return false;
    }
    if (password.search(/[A-Z]/) === -1) {
        // console.log(password.search(/[a-z]/))
        createAlert("Password should be min 6 character with mix of Uppercase, lowercase and digits");
        return false;
    }
    return true;
}

function validate(userData){
    if(userData.email==='' || userData.password==='' || userData.permissions.length===0){
        createAlert("All the fields are required");
        return;
    }

    if(!validateEmail(userData.email)){
        createAlert("Please enter a valid email id")
        return;
    }
    
    if(!validatePassword(userData.password)){
        return;
    }

    console.log("reached edn")
    if(userData.permissions.length < 2){
        createAlert("Atleast two Permissions are required.")
        return;
    }

    form.style.display="none";
    let element = document.getElementById("details");
    element.classList.remove("hidden");
    displayDetails(userData)
}

form.addEventListener('submit', (event) => {
    event.preventDefault();
    let userData = {}
    let email    = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let gender   = document.getElementById("gender").value

    let role;
    if (document.getElementById('user').checked) {
        role = document.getElementById('user').value;
    }
    else if(document.getElementById('admin').checked){
        role = document.getElementById('admin').value;
    }

    let permissions=[]
    if(document.getElementById("perm1").checked)
    {
        let val = document.getElementById("perm1").value
        permissions.push(val)
    }
    if(document.getElementById("perm2").checked)
    {
        let val = document.getElementById("perm2").value
        permissions.push(val)
    }
    if(document.getElementById("perm3").checked)
    {
        let val = document.getElementById("perm3").value
        permissions.push(val)
    }
    if(document.getElementById("perm4").checked)
    {
        let val = document.getElementById("perm4").value
        permissions.push(val)
    }
    userData.email       = email.trim()
    userData.password    = password.trim()
    userData.gender      = gender
    userData.role        = role
    userData.permissions = permissions
    
    validate(userData)
    
});
